import Intent from './Intent';

export default interface Dialog {
  intents: Intent[];
}
