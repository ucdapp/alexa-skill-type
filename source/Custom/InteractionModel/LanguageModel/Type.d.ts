import Value from './Value';

export default interface Type {
  name: string;
  values: Value[];
}
