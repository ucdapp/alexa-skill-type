/** @link https://developer.amazon.com/docs/device-apis/alexa-property-schemas.html#fooditem */

// @formatter:off
declare const enum FoodCategory {
  BEEF = 'BEEF',
  BEVERAGE = 'BEVERAGE',
  CHICKEN = 'CHICKEN',
  FISH = 'FISH',
  MEAT = 'MEAT',
  PASTA = 'PASTA',
  PIZZA = 'PIZZA',
  POPCORN = 'POPCORN',
  PORK = 'PORK',
  POTATO = 'POTATO',
  SHRIMP = 'SHRIMP',
  SOUP = 'SOUP',
  TURKEY = 'TURKEY',
  WATER = 'WATER',
  UNKNOWN = 'UNKNOWN',
}
// @formatter:on

export default FoodCategory;
