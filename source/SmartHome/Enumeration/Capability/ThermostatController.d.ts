// @formatter:off

export const enum ThermostatControllerPayloadName {
  targetSetpoint = 'targetSetpoint',
  lowerSetpoint = 'lowerSetpoint',
  upperSetpoint = 'upperSetpoint',
  targetSetpointDelta = 'targetSetpointDelta',
  thermostatMode = 'thermostatMode',
}

export const enum ThermostatControllerRequestName {
  SetTargetTemperature = 'SetTargetTemperature',
  AdjustTargetTemperature = 'AdjustTargetTemperature',
  SetThermostatMode = 'SetThermostatMode'
}

export const enum ThermostatControllerPropertyName {
  targetSetpoint = 'targetSetpoint',
  lowerSetpoint = 'lowerSetpoint',
  upperSetpoint = 'upperSetpoint',
  thermostatMode = 'thermostatMode',
}

// @formatter:on
