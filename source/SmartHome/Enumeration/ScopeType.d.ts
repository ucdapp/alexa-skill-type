/** @link https://developer.amazon.com/docs/device-apis/message-guide.html#scope-object */

// @formatter:off
declare const enum ScopeType {
  BearerToken = 'BearerToken',
  BearerTokenWithPartition = 'BearerTokenWithPartition',
}
// @formatter:on

export default ScopeType;
