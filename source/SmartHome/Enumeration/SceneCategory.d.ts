/** @link https://developer.amazon.com/docs/smarthome/provide-scenes-in-a-smart-home-skill.html#allowed-devices */

// @formatter:off
declare const enum SceneCategory {
  LIGHT = 'LIGHT',
  SPEAKER = 'SPEAKER',
  SMARTPLUG = 'SMARTPLUG',
  SWITCH = 'SWITCH',
  THERMOSTAT = 'THERMOSTAT',

  // ACTIVITY_TRIGGER = 'ACTIVITY_TRIGGER',
  // CAMERA = 'CAMERA',
  // DOOR = 'DOOR',
  // OTHER = 'OTHER',
  // SCENE_TRIGGER = 'SCENE_TRIGGER',
  // SMARTLOCK = 'SMARTLOCK',
  // TEMPERATURE_SENSOR = 'TEMPERATURE_SENSOR',
  // TV = 'TV',
}
// @formatter:on

export default SceneCategory;
