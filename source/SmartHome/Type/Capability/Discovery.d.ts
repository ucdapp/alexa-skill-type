import DisplayCategory from '../../Enumeration/DisplayCategory';
import SkillNamespace from '../../Enumeration/SkillNamespace';
import Header from '../Header';
import Scope from '../Scope';

export interface DiscoveryRequest {
  directive: { header: Header; payload: { scope: Scope; }; };
}

export interface SupportedItem {
  name: string;
}

export interface CapabilityItem {
  interface: SkillNamespace;
  properties?: { supported?: SupportedItem[]; proactivelyReported?: boolean; retrievable?: boolean; };
  supportedOperations?: string[];
  type: string;
  version: string;
}

export interface DiscoveryEndpoint {
  capabilities: CapabilityItem[];
  cookie?: { [index: string]: any };
  description: string;
  displayCategories: DisplayCategory[];
  endpointId: string;
  friendlyName: string;
  manufacturerName: string;
}

export interface DiscoveryResponse {
  event: { header: Header; payload: { endpoints: DiscoveryEndpoint[]; }; };
}

