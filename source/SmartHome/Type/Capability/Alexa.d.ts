// @formatter:off
declare const enum AlexaName {
  ReportState = 'ReportState',
  ChangeReport = 'ChangeReport',
  DeferredResponse = 'DeferredResponse',
  ErrorResponse = 'ErrorResponse',
  Response = 'Response',
  StateReport = 'StateReport',
}
// @formatter:on

export default AlexaName;
