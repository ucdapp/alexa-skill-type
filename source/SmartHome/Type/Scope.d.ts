import ScopeType from '../Enumeration/ScopeType';

export default interface Scope {
  token: string;
  type: ScopeType;

  [index: string]: string;
}

export interface BearerTokenWithPartitionScope extends Scope {
  partition: string;
  userId: string;
}
