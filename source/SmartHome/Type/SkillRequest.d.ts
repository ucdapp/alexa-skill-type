import Endpoint from './Endpoint';
import Header from './Header';
import Scope from './Scope';

export default interface SkillRequest {
  directive: {
    header: Header; payload: { scope?: Scope; [index: string]: any }; endpoint?: Endpoint;
  };
}
